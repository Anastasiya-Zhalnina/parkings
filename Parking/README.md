### Техническое задание
___
* Необходимо разработать серверную часть по приёму данных о машиноместах от парковочного оборудования. Принятые данные 
должны добавляться в таблицу БД `PLACES`, не удаляя и не изменяя прошлые значения.
  
* Необходимо принимать данные о машиноместах только для парковок из таблицы `PARKINGS` и для тех из них, которые 
находятся в состоянии `Active`

* Таблица `PLACES`
```
Id                VARCHAR(30), KEY
Parking_Number    Number(6)
Client_Free       Number(4)
Client_Busy       Number(4)
Vip_Free          Number(4)
Vip_Busy          Number(4)
Reserved_Free     Number(4)
Reserved_Busy     Number(4)
Timestamp         DateTime
```

* Таблица `PARKINGS`
```
Id                VARCHAR(30), KEY
Parking_Number    Number(6)
Active            Boolean
```

* Протокол обмена: HTTP
```
Метод: POST
Accept: application/json
Content-type: application/json
```
* Входящий запрос на сервер:
```
  Наименование      Тип         Обязательно       Описание

type              String      Да                Должно быть "places"
parking_number    Integer     Да                Номер парковки по ЦМИУ
client_free       Integer     Да                Количество свободных мест (разовые клиенты)
client_busy       Integer     Да                Количество занятых мест (разовые клиенты)
vip_free          Integer     Да                Количество свободных мест(инвалиды)
vip_busy          Integer     Да                Количество занятых мест(инвалиды)
resereved_free    Integer     Да                Количество свободных мест (зарезервированные места)
reserved_busy     Integer     Да                Количество занятых мест (зарезервированные места)
date_event        DateTime    Да                Время события
```
* Исходящий ответ:
```
Наименование      Тип         Обязательно       Описание
type              String      Да                Должно быть "places"
parking_number    Integer     Да                Номер парковки по ЦМИУ
date_event        Date        Да                Время ответа
error             Integer     Да                Код ошибки
```

* Код ошибки ("error"):
```
0 – нет ошибок
1 – есть ошибка
```
### Используемый стек технологий
___
* SpringBoot
* Hibernate
* REST API
* PostgreSQL
* Lombok
* Spring Data JPA

### Запуск
___
* src/main/resources/http/request.http 

Пример входящего запроса:
```http request
POST localhost/places
Accept: application/json
Content-Type: application/json

{
  "type": "places",
  "parking_number": "1",
  "client_free": "1",
  "client_busy": "1",
  "vip_free": "1",
  "vip_busy": "1",
  "reserved_free": "1",
  "reserved_busy": "1",
  "date_event": "2022-04-01T09:49:19.275039200"
}
```
Исходящий ответ, в случае для парковок, которые находятся в состоянии `ACTIVE` и в таблице `PARKINGS`. Таблица `PLACES`
обновлена.
```json
{
  "type": "places",
  "parking_number": 1,
  "date_event": "2022-04-02 16:38:23",
  "error": 0
}
```
Исходящий ответ, в случае для парковок, которые не находятся в состоянии `ACTIVE` или запрос был отправлен с ошибкой.
Таблица `PLACES` осталась без изменений.
```json
{
  "type": "places",
  "parking_number": 1,
  "date_event": "2022-04-02 16:38:23",
  "error": 1
}
```


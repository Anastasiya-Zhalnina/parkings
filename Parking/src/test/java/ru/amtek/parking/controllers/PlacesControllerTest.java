package ru.amtek.parking.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.amtek.parking.dto.PlacesDto;
import ru.amtek.parking.dto.ResponseDto;
import ru.amtek.parking.services.PlacesService;

import java.time.LocalDateTime;
import java.util.Date;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@DisplayName("PlacesController is working when")
class PlacesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlacesService placesService;

    @BeforeEach
    public void setUp() {

        when(placesService.addPlace(
                PlacesDto.builder()
                        .parkingNumber(1)
                        .clientFree(1)
                        .clientBusy(1)
                        .vipFree(1)
                        .vipBusy(1)
                        .reservedFree(1)
                        .reservedBusy(1)
                        .dateEvent(LocalDateTime.parse("2022-04-02T09:49:19.275039200"))
                        .build()))
                .thenReturn(ResponseDto.builder()
                        .type("places")
                        .parkingNumber(1)
                        .dateEvent(new Date())
                        .error(0)
                        .build());
    }

    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addPlace() is working")
    class AddPlaceTest {
        @Test
        public void add_place() throws Exception {
            mockMvc.perform(post("/places")
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                    "  \"type\": \"places\",\n" +
                                    "  \"parking_number\": \"1\",\n" +
                                    "  \"client_free\": \"1\",\n" +
                                    "  \"client_busy\": \"1\",\n" +
                                    "  \"vip_free\": \"1\",\n" +
                                    "  \"vip_busy\": \"1\",\n" +
                                    "  \"reserved_free\": \"1\",\n" +
                                    "  \"reserved_busy\": \"1\",\n" +
                                    "  \"date_event\": \"2022-04-02T09:49:19.275039200\"\n" +
                                    "}"))
                    .andExpect(status().is(HttpStatus.CREATED.value()))
                    .andExpect(jsonPath("$.type", is("places")))
                    .andExpect(jsonPath("$.parking_number", is(1)))
                    .andExpect(jsonPath("$.error", is(0)));
        }
    }
}
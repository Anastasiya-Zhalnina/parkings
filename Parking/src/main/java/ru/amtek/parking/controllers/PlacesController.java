package ru.amtek.parking.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.amtek.parking.dto.PlacesDto;
import ru.amtek.parking.dto.ResponseDto;
import ru.amtek.parking.services.PlacesService;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/places")
public class PlacesController {
    private final PlacesService placesService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDto addPlace(@RequestBody PlacesDto place) {
        return placesService.addPlace(place);
    }
}

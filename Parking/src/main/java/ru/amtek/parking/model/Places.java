package ru.amtek.parking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Places {
    @Id
    private String id;

    @JoinColumn(name = "parking_number")
    private Integer parkingNumber;

    @JoinColumn(name = "client_free")
    private Integer clientFree;

    @JoinColumn(name = "client_busy")
    private Integer clientBusy;

    @JoinColumn(name = "vip_free")
    private Integer vipFree;

    @JoinColumn(name = "vip_busy")
    private Integer vipBusy;

    @JoinColumn(name = "reserved_free")
    private Integer reservedFree;

    @JoinColumn(name = "reserved_ busy")
    private Integer reservedBusy;

    @JoinColumn(name = "date_event")
    private LocalDateTime dateEvent;
}

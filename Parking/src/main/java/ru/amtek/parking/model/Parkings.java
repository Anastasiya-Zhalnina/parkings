package ru.amtek.parking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Parkings {

    @Id
    private String id;

    @JoinColumn(name = "parking_number")
    private Integer parkingNumber;

    private Boolean active;
}

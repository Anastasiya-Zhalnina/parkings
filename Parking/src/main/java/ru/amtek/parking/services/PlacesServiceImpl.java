package ru.amtek.parking.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.amtek.parking.dto.PlacesDto;
import ru.amtek.parking.dto.ResponseDto;
import ru.amtek.parking.exception.NotActiveException;
import ru.amtek.parking.model.Parkings;
import ru.amtek.parking.model.Places;
import ru.amtek.parking.repositories.ParkingsRepository;
import ru.amtek.parking.repositories.PlacesRepository;

import java.util.UUID;

import static ru.amtek.parking.dto.ResponseDto.from;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@Service
@RequiredArgsConstructor
public class PlacesServiceImpl implements PlacesService {

    public static final int ERROR_CODE = 1;
    private final PlacesRepository placesRepository;
    private final ParkingsRepository parkingsRepository;


    @Override
    public ResponseDto addPlace(PlacesDto place) {
        String id = generationId();
        Places newPlace = Places.builder()
                .id(id)
                .parkingNumber(place.getParkingNumber())
                .clientFree(place.getClientFree())
                .clientBusy(place.getClientBusy())
                .vipFree(place.getVipFree())
                .vipBusy(place.getVipBusy())
                .reservedFree(place.getReservedFree())
                .reservedBusy(place.getReservedBusy())
                .dateEvent(place.getDateEvent())
                .build();
        try {
            Parkings parking = parkingsRepository.findByParkingNumber(place.getParkingNumber());
            if (parking.getActive()) {
                placesRepository.save(newPlace);
            } else {
                throw new NotActiveException();
            }
        } catch (Exception e) {
            ResponseDto response = from(newPlace);
            response.setError(ERROR_CODE);
            return response;
        }
        return from(newPlace);
    }


    public String generationId() {
        String generationId = UUID.randomUUID().toString().replace("-", "");
        return generationId.substring(1, generationId.length() - 1);
    }
}

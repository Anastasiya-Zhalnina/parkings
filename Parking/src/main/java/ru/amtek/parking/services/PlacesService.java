package ru.amtek.parking.services;

import ru.amtek.parking.dto.PlacesDto;
import ru.amtek.parking.dto.ResponseDto;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

public interface PlacesService {
    ResponseDto addPlace(PlacesDto place);
}

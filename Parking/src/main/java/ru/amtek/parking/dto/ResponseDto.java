package ru.amtek.parking.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.amtek.parking.model.Places;

import java.util.Date;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseDto {

    private static final String PLACES_TYPE = "places";
    private static final int ERROR_CODE = 0;

    @JsonProperty("type")
    private String type;

    @JsonProperty("parking_number")
    private Integer parkingNumber;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Moscow")
    @JsonProperty("date_event")
    private Date dateEvent;

    @JsonProperty("error")
    private Integer error;

    public static ResponseDto from(Places place) {
        return ResponseDto.builder()
                .type(PLACES_TYPE)
                .parkingNumber(place.getParkingNumber())
                .dateEvent(new Date())
                .error(ERROR_CODE)
                .build();
    }
}

package ru.amtek.parking.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlacesDto {

    @JsonProperty("parking_number")
    private Integer parkingNumber;

    @JsonProperty("client_free")
    private Integer clientFree;

    @JsonProperty("client_busy")
    private Integer clientBusy;

    @JsonProperty("vip_free")
    private Integer vipFree;

    @JsonProperty("vip_busy")
    private Integer vipBusy;

    @JsonProperty("reserved_free")
    private Integer reservedFree;

    @JsonProperty("reserved_busy")
    private Integer reservedBusy;

    @JsonProperty("date_event")
    private LocalDateTime dateEvent;
}

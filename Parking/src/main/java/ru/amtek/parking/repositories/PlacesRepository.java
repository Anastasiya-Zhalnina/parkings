package ru.amtek.parking.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.amtek.parking.model.Places;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

public interface PlacesRepository extends JpaRepository<Places, String> {
}

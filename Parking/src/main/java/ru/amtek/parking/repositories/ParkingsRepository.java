package ru.amtek.parking.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.amtek.parking.model.Parkings;

/**
 * Parking
 * 02.04.2022
 *
 * @author Anastasiya Zhalnina
 */

public interface ParkingsRepository extends JpaRepository<Parkings, String> {
    Parkings findByParkingNumber(Integer parkerNumber);
}

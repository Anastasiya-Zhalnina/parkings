create table places
(
    id             varchar(30) primary key,
    parking_number numeric(6),
    client_free    numeric(4),
    client_busy    numeric(4),
    vip_free       numeric(4),
    vip_busy       numeric(4),
    reserved_free  numeric(4),
    reserved_busy  numeric(4),
    date_event     timestamp
);

create table parkings
(
    id             varchar(30) primary key,
    parking_number numeric(6),
    active         boolean
);